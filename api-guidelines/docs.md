## Docs [D] <a name="docs"></a>

These are rules about the public docs (JavaDocs) for APIs.

### All public APIs must be documented <a name="obviously"></a>

All public APIs must have sufficient javadocs to explaining how a developer
would use the API. Assume the developer found it via auto-complete or while
browsing through API reference docs and has a minimal amount of context from the
adjacent API surface (ex. on the same class).

### Methods

Method parameters and return values must be documented using `@param` and
`@return` docs annotations, respectively. The javadoc body should be formatted
as though it is preceded by "This method...".

In cases where a method takes no parameters, has no special considerations, and
simply returns what the method name says it does, the `@return` may be omitted
and docs may be written similar to:

```java {.good .no-copy}
/**
 * Returns the priority of the thread.
 */
@IntRange(from = 1, to = 10)
public int getPriority() { ... }
```

### Always use links in JavaDocs <a name="links"></a>

Docs should link to other docs for related constants, methods, etc. Use Javadoc
tags (e.g., `@see` and `{@link foo}`), not just plain-text words.

For:

```java {.no-copy}
public static final int FOO = 0;
public static final int BAR = 1;
```

Follow:

```java {.bad .no-copy}
/**
 * Sets value to one of FOO or <code>BAR</code>.
 *
 * @param value the value being set, one of FOO or BAR
 */
public void setValue(int value) { ... }
```

```java {.good .no-copy}
/**
 * Sets value to one of {@link ##FOO} or {@link ##BAR}.
 *
 * @param value the value being set
 */
public void setValue(@ValueType int value) { ... }
```

Note that using an `IntDef` annotation such as `@ValueType` on a parameter will
automatically generate documentation specifying the allowed types. See the
guidance on [annotations](#annotations-intdef) for more information on `IntDef`.

### Run update-api or docs target when adding JavaDocs <a name="update-api"></a>

This rule is particularly important when adding `@link` or `@see` tags, and make
sure the output looks as expected. It is common to see ERROR output in JavaDocs
from bad links. Either the `update-api` or `docs` Make target will perform this
check, but the `docs` target might be quicker if you are simply changing
javadocs and do not otherwise need to run the `update-api` target.

### Use `{@code foo}` to distinguish Java values <a name="code-font"></a>

Java values like `true`, `false`, and `null` should be wrapped with `{@code
...}` to distinguish them from documentation text.

### `@param` and `@return` summaries should be a single sentence fragment <a name="param-return"></a>

Parameter and return value summaries should start with a lowercase character and
contain only a single sentence fragment. If you have additional information that
extends beyond a single sentence, move it to the method javadoc body.

```java {.bad .no-copy}
/**
 * @param e The element to be appended to the list. This must not be
 *       null. If the list contains no entries, this element will
 *       be added at the beginning.
 * @return This method returns true on success.
 */
```

```java {.good .no-copy}
/**
 * @param e element to be appended to this list, must be non-{@code null}
 * @return {@code true} on success, {@code false} otherwise
 */
```

### Docs annotations need explanations <a name="annotations"></a>

Annotations `@hide` and `@removed` should include documentation as to why they
are hidden from public API. Use of `@deprecated` annotation must include
instructions on how to replace usages of the deprecated API.

### Use `@throws` to document exceptions <a name="throws"></a>

If a method throws a checked exception, for example `IOException`, the exception
must be documented with `@throws`. For Kotlin-sourced APIs intended for use by
Java clients, annotate functions with
[`@Throws`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.jvm/-throws/).

If a method throws an unchecked exception indicating a preventable error, for
example `IllegalArgumentException` or `IllegalStateException`, the exception
must be documented with an explanation of why the exception is thrown. The
thrown exception should also indicate why it was thrown.

```java {.good .no-copy}
/**
 * ...
 * @throws IOException If it cannot find the schema for {@code toVersion}
 * @throws IllegalStateException If the schema validation fails
 */
public SupportSQLiteDatabase runMigrationsAndValidate(String name, int version,
    boolean validateDroppedTables, Migration... migrations) throws IOException {
  // ...
  if (!dbPath.exists()) {
    throw new IllegalStateException("Cannot find the database file for " + name
        + ". Before calling runMigrations, you must first create the database "
        + "via createDatabase.");
  }
  // ...
```

```kotlin {.good .no-copy}
/**
 * ...
 * @throws IOException If something goes wrong reading the file, such as a bad
 *                     database header or missing permissions
 */
@Throws(IOException::class)
fun readVersion(databaseFile: File): Int {
  // ...
  val read = input.read(buffer)
    if (read != 4) {
      throw IOException("Bad database header, unable to read 4 bytes at " +
          "offset 60")
    }
  }
  // ...
```

If the method invokes asynchronous code that may throw exceptions, please
consider how the developer will find out about and respond to such exceptions.
Typically this involves forwarding the exception to a callback and documenting
the exceptions thrown on the method that receives them. Asynchronous exceptions
should not be documented with `@throws` unless they are actually re-thrown from
the annotated method.

### End the first sentence of docs with a period <a name="period"></a>

The doclava tool parses docs simplistically, ending the synopsis doc (the first
sentence, used in the quick description at the top of the class docs) as soon as
it sees a period (.) followed by a space. There are two problems that this
causes:

*   If a short doc is not ended with a period, and if that member has inherited
    docs that are picked up by the tool, then the synopsis also picks up those
    inherited docs. See, for example, actionBarTabStyle in the
    [R.attr docs](https://developer.android.com/reference/android/R.attr.html),
    which has the description of the dimension added into the synopsis.
*   Avoid “e.g.” in the first sentence for the same reason, because doclava will
    end the synopsis docs after “g.”. See, for example, `TEXT_ALIGNMENT_CENTER`
    in
    [View.java](https://developer.android.com/reference/android/view/View.html).
    Note that Metalava will automatically correct this error by inserting a
    non-breaking space after the period; however, please don’t make this mistake
    in the first place.

### Format docs to be rendered in HTML <a name="html"></a>

Javadocs will be rendered in HTML, so format them accordingly:

*   Line breaks should use an explicit `<p>` tag. Do not add a closing `</p>`
    tag.

*   **Do not use ASCII to render lists or tables.**

*   Lists should use `<ul>` or `<ol>` for unordered and ordered, respectively.
    Each item should begin with a `<li>` tag, but does not need a closing
    `</li>` tag. A closing `</ul>` or `</ol>` tag is required after the last
    item.

*   Tables should use `<table>`, `<tr>` for rows, `<th>` for headers, and `<td>`
    for cells. All table tags require matching closing tags. You may use
    `class="deprecated"` on any tag to denote deprecation.

*   To create inline code font, use `{@code foo}`.

*   To create code blocks, use `<pre>`.

*   All text inside a `<pre>` block is parsed by the browser, so be careful with
    brackets `<>`. You can escape them with `&lt;` and `&gt;` HTML entities.

*   Alternatively, you can leave raw brackets `<>` in your code snippet if you
    wrap the offending sections in `{@code foo}`. For example:

    ``` {.no-copy}
    <pre>{@code <manifest>}</pre>
    ```

### Follow the API reference style guide <a name="style-guide"></a>

To ensure consistency in the style for class summaries, method descriptions,
parameter descriptions, etc., follow the recommendations in the official Java
language guidelines at
[How to Write Doc Comments for the Javadoc Tool](http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html).
